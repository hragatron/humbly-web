# Humbly web player
This is a work in progress.

# Running
1. Clone
2. run `npm i` from the cloned directory
3. run `npm run start`
4. A dev url should appear, should be `localhost:3000`. Clicking on that will give you errors since URL params are necessary at this time (will be gracefully handled in the fututre)
5. assuming the port is `:3000`, you can use [this URL](http://localhost:3000/?time=0&artwork_source=https://cdn-images-1.listennotes.com/podcasts/song-exploder-song-exploder-Ut3fuw2xdYv.1400x1400.jpg&show_name=Song%20Exploder&episode_title=alt-J%20-%20In%20Cold%20Blood&audio_source=https://www.listennotes.com/e/p/b36546bb573e428db2d56e71257281f1/) locally.

# Experiencing
* All basic functionalty as far as playback goes works.
* It's also a responsive layout, so feel free to try it on large and small screen sizes. * Scrubbing works well
* The "share" button at the bottom is not currently hooked up

##### Thanks
– Hrag ✌️