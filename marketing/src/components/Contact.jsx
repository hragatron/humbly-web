import React from 'react';
import { NavLink } from 'react-router-dom';

import '../styles/contact.scss';

class Contact extends React.Component {
  constructor(){
    super();
    this.state = {
      isSending: false,
      justSent: false
    }
    this.handleSend = this.handleSend.bind(this);
  }

  componentDidMount() {
    document.body.setAttribute('data-section', 'contact');
    this.props.setNavBgClass('colorful-section');
    this.props.a11yNavHandler('Navigated to Contact');
  }

  handleSend(e) {
    e.preventDefault();
    this.setState({
      isSending: true
    })

    const url = 'https://us-central1-humbly-app-7410e.cloudfunctions.net/contactFormDev'
    const target = e.target;
    const body = {
      name: target.name.value,
      email: target.email.value,
      bodyHTML: target.body.value,
      type: 'contact'
    }

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      mode: 'cors',
      body: JSON.stringify(body)
    }).then(res => {
      this.setState({
        isSending: false,
        justSent: true
      }, () => {
        setTimeout(() => {
          this.setState({
            justSent: false
          })
        }, 5000);
      })
    }).catch(err => {
      this.setState({
        isSending: false
      })
      console.error(err)
    });
  }

  determineButtonContents() {
    if (this.state.isSending) {
      return <div className="loading-thin"></div>;
    } else if (this.state.justSent) {
      return <span>Sent! <span role="img" aria-label="Thumbs up emoji">👍</span></span>;
    } else {
      return "Send";
    }
  }

  render() {
    return (
      <div className="inner contact">
        <div className="contact-heading">
          <h2>Hellos <span role="img" aria-label="handshake emoji">👋</span></h2>
          <p>
            Use this form to contact Humbly about anything.<br/>
            Or use <NavLink to="/inquiry">this form</NavLink> to contact Humbly about Advertising.
          </p>
        </div>
        <form onSubmit={ this.handleSend }>
          <div className="contact-wrapper grid">
            <div className="contact-item">
              <label htmlFor="name">Name</label>
              <input id="name" name="name" required />
            </div>

            <div className="contact-item">
              <label htmlFor="email">Email address</label>
              <input id="email" type="email" name="email" required />
            </div>

            <div className="contact-item mod-fullwidth">
              <label htmlFor="body">What's up?</label>
              <textarea id="body" name="body" required></textarea>
            </div>

            <div className="contact-submit">
              <button type="submit" disabled={this.state.isSending || this.state.justSent ? true : false} className="button button-regular">
                {this.determineButtonContents()}
              </button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default Contact;